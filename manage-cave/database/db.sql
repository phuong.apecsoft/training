-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 23, 2020 lúc 04:30 AM
-- Phiên bản máy phục vụ: 10.1.31-MariaDB
-- Phiên bản PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cv`
--

CREATE TABLE `cv` (
  `id` int(10) NOT NULL,
  `tencv` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `idtime` varchar(12) NOT NULL,
  `img` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `cv`
--

INSERT INTO `cv` (`id`, `tencv`, `price`, `idtime`, `img`) VALUES
(9, 'Ngọc Trinh', 30000000, '10h-12h ', 'uploads/nt.jpg'),
(10, 'Hương Mơ', 400000, '8h-10h', 'uploads/nt.jpg'),
(11, 'Ngọc Trinh', 30000000, '10h-12h ', 'uploads/nt.jpg'),
(12, 'Hương Mơ', 400000, '10h-12h ', 'uploads/ellytran-thuytop-phunutoday_vn-1.jpg'),
(13, 'Ngọc Trinh', 30000000, '8h-10h', 'uploads/nt.jpg'),
(14, 'Hương Mơ', 400000, '10h-12h ', 'uploads/ellytran-thuytop-phunutoday_vn-1.jpg'),
(17, 'Ngọc Trinh', 30000000, '10h-12h', 'uploads/nt.jpg'),
(18, 'Ngọc Trinh', 30000000, '18h-20h', 'uploads/nt.jpg'),
(20, 'maria ozawa', 30000000, '10h-12h ', 'uploads/unnamed.jpg'),
(21, 'maria ozawa', 300000, '10h-12h', 'uploads/unnamed.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khachsan`
--

CREATE TABLE `khachsan` (
  `idks` int(4) NOT NULL,
  `tenks` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `price` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `khachsan`
--

INSERT INTO `khachsan` (`idks`, `tenks`, `address`, `price`) VALUES
(1, 'Thiên đường mộng mơ', '15 Trần Duy Hưng', 400000),
(2, 'Chiều tím', '145 Trần Duy Hưng', 500000),
(3, 'Thiên đường mộng mơ', '15 Trần Duy Hưng', 300000),
(4, 'Chiều tím', '145 Trần Duy Hưng', 350000),
(5, 'Thiên đường mộng mơ', '15 Trần Duy Hưng', 300000),
(6, 'Chiều tím', '145 Trần Duy Hưng', 350000),
(7, 'Thiên đường mộng mơ', '15 Trần Duy Hưng', 300000),
(8, 'Chiều tím', '145 Trần Duy Hưng', 450000),
(9, 'Thiên đường mộng mơ', '15 Trần Duy Hưng', 300000),
(10, 'Chiều tím', '145 Trần Duy Hưng', 500000),
(11, 'Ánh Hoa', '26 Nguyễn Trãi', 300000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(4) NOT NULL,
  `idcv` int(4) NOT NULL,
  `idtime` varchar(12) NOT NULL,
  `idks` int(4) NOT NULL,
  `price` int(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `idcv`, `idtime`, `idks`, `price`, `name`, `email`, `phone`) VALUES
(84, 14, '8h-10h', 10, 0, 'Hai', 'hainv@gmail.com', 4534534),
(85, 17, '8h-10h', 3, 0, 'Huy', 'huytt@gmail.com', 3242343),
(86, 14, '8h-10h', 10, 0, 'Hai', 'hainv@gmail.com', 4534534),
(87, 17, '8h-10h', 3, 0, 'Huy', 'huytt@gmail.com', 3242343),
(88, 14, '8h-10h', 10, 0, 'Hai', 'hainv@gmail.com', 4534534),
(89, 17, '8h-10h', 3, 0, 'Huy', 'huytt@gmail.com', 3242343),
(90, 14, '8h-10h', 10, 0, 'Hai', 'hainv@gmail.com', 4534534),
(91, 17, '8h-10h', 3, 0, 'Huy', 'huytt@gmail.com', 3242343),
(100, 11, '12h-14h', 1, 0, '30000000', 'phuongk3a@gmail.com', 976684362),
(140, 9, '22h-8h', 2, 30000000, 'TIch', 'tichtd@gmail.com', 976684575),
(144, 12, '10h-12h', 1, 400000, '', 'sdsf', 0),
(145, 21, '10h-12h', 1, 300000, 'Minh', 'minhtr@gmail.com', 254234);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `times`
--

CREATE TABLE `times` (
  `idtime` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `times`
--

INSERT INTO `times` (`idtime`) VALUES
('10h-12h'),
('12h-14h'),
('14h-16h'),
('16h-18h'),
('18h-20h'),
('20h-22h'),
('22h-8h'),
('8h-10h');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `cv`
--
ALTER TABLE `cv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idtime` (`idtime`);

--
-- Chỉ mục cho bảng `khachsan`
--
ALTER TABLE `khachsan`
  ADD PRIMARY KEY (`idks`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idcv` (`idcv`),
  ADD KEY `idtime` (`idtime`),
  ADD KEY `idks` (`idks`);

--
-- Chỉ mục cho bảng `times`
--
ALTER TABLE `times`
  ADD PRIMARY KEY (`idtime`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `cv`
--
ALTER TABLE `cv`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `khachsan`
--
ALTER TABLE `khachsan`
  MODIFY `idks` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `cv`
--
ALTER TABLE `cv`
  ADD CONSTRAINT `cv_ibfk_1` FOREIGN KEY (`idtime`) REFERENCES `times` (`idtime`);

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`idcv`) REFERENCES `cv` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`idtime`) REFERENCES `times` (`idtime`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`idks`) REFERENCES `khachsan` (`idks`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
