<?php
Class User extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->database();   
    }   

    function getall($table){
          return $this->db->get($table)->result_array();
    }
   
    function getsum(){
        $this->db->select_sum('sotien');
        $this->db->where("MONTH(ngay)", 5);
        $query = $this->db->get('ds_thuchi');
        /*
        cach 2: 
        $sql = "select sum(sotien) from ds_thuchi";
        $result = $this->db->query($sql);
        */
        return $query->row();
    }


    function total_month($month){
        $this->db->select_sum('sotien');
        $this->db->where("MONTH(ngay)", $month);
        $query = $this->db->get('ds_thuchi');
        return $query->row();
    }
    function getall_join(){
        $this->db->select('*');
        $this->db->from('ds_thuchi'); 
        $this->db->join('user', 'ds_thuchi.user = user.name');
        $this->db->order_by('ngay', 'asc');
       return $this->db->get()->result();
     //  return $this->db->get(); tra ket qua theo cach 2
    }

    function update()
    {
        $data = array(
            'fullname' => 'My title',
            'email'  => 'My Name',
            'phone'  => '2342423'
        );
        $this->db->where('id', 5);
        $this->db->update('student', $data);
    
    }
    function delete($id){
        $this->db->where('id ='. $id);
        $this->db->delete('ds_thuchi');
     // $ds_thuchi =  $this->getbyid($id);
     // $this->db->delete('ds_thuchi', array('id' => $id));
      
    }
    function delete_dsthuchi($id){
        $this->db->where('id ='. $id);
        $this->db->delete('ds_thuchi'); 
    }
    /*
    function add($data){
        $this->db->insert('ds_thuchi', $data);
    }
    */
    function add_dsthuchi($data){
        $this->db->insert('ds_thuchi', $data);
    }


    function getbyid($id){
        $this->db->where('id =' .$id);
        return $this->db->get('ds_thuchi')->result_array()['0'];
    }
    function edit($id, $data){
        $this->db->where('id =', $id);
        $this->db->update('ds_thuchi', $data);
      //  $this->db->update('ds_thuchi', array_filter($data), "id = ". $id) ;
    }
    function edit_dsthuchi($id, $data){
       // $this->db->where('id =', $id);
       //$t= $this->db->update('ds_thuchi', $data);

        $this->db->update('ds_thuchi', array_filter($data), "id = ". $id) ;
    }


}

?>