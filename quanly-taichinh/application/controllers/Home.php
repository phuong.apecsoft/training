<?php
Class Home extends CI_Controller
{
	/*	
    function index()
    {	
		$this->load->model('user');
		$data['listthuchi'] = $this->user->getall('ds_thuchi');
		var_dump ($data);
		die;
		$this->load->view('main_layout', $data);
	}
	*/
	function index()
    {	
		$this->load->model('user');
		$data['query'] =	$this->user->getall_join();
		$this->load->view('dsthuchi', $data);
	}
	function total()
    {	
		$this->load->model('user');
		$data['query'] =	$this->user->getall_join();
		//$data['total'] = $this->user->getsum();
		$data['total1'] = $this->user->total_month(1);
		$data['total2'] = $this->user->total_month(2);
		$data['total3'] = $this->user->total_month(3);
		$data['total4'] = $this->user->total_month(4);
		$data['total5'] = $this->user->total_month(5);
		$data['total6'] = $this->user->total_month(6);
		$this->load->view('total', $data);
		/*
		echo ('<prev>');
			var_dump($data['total']);
		echo ('</prev>');
		
		*/
	}

	function update()
    {	
		$this->load->model('user');
		$this->user->update();
	}

	function delete(){
		$id = $this->uri->segment(3);
		$this->load->model('user');
		$this->user->delete($id);
		redirect('Home');	
	}
	function delete_dsthuchi(){
		$id = $this->uri->segment(3);
		$this->load->model('user');
		$this->user->delete_dsthuchi($id);
		redirect('Home');	
	}
	function add(){
		$this->load->view('add');
		if($this->input->post()){
			$data  = $this->input->post();
			$this->load->model('user');
			$this->user->add($data);
			redirect('Home');	
		}
	}
	function add_dsthuchi(){
		$this->load->model('user');
		$data['listthuchi'] = $this->user->getall('user');
		$data['listdanhmuc'] = $this->user->getall('danhmuc');
		$this->load->view('add_dsthuchi', $data);
		if($this->input->post()){
			$data  = $this->input->post();
			$this->load->model('user');
			$this->user->add_dsthuchi($data);
			redirect('Home');	
		}
	}

	function edit(){
		$id = $this->uri->segment(3);
		$this->load->model('user');
		//$data['listNhanSu'] = $this->user->getall();
		$data['data_update'] = $this->user->getbyid($id);
		$this->load->view('edit', $data);

		if(isset($_POST['btn-edit'])){
			$data  = $this->input->post();
			$this->user->edit($id, $data);
		
		}
	}

	function edit_dsthuchi(){
		$id = $this->uri->segment(3);
		$this->load->model('user');
		$data['data_update'] = $this->user->getbyid($id);

		$data['listuser'] = $this->user->getall('user');
		$data['listdanhmuc'] = $this->user->getall('danhmuc');
		$this->load->view('edit_dsthuchi', $data);	
		if(isset($_POST['btn-edit'])){
			$data  = $this->input->post();
			$this->user->edit_dsthuchi($id, $data);
			redirect('Home');	
		}
	}
}
?>
