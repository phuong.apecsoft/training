-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 02, 2020 lúc 12:00 PM
-- Phiên bản máy phục vụ: 10.1.31-MariaDB
-- Phiên bản PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `quanly-taichinh`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `danhmuc`
--

CREATE TABLE `danhmuc` (
  `id` int(4) NOT NULL,
  `tendm` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `danhmuc`
--

INSERT INTO `danhmuc` (`id`, `tendm`) VALUES
(1, 'Tiền học của con'),
(2, 'Tiền sữa'),
(3, 'Tiền điện'),
(4, 'Tiền nước'),
(5, 'Tiền chi tiêu hàng ngày'),
(6, 'tiền mạng'),
(7, 'tiền lương');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ds_thuchi`
--

CREATE TABLE `ds_thuchi` (
  `id` int(4) NOT NULL,
  `ten` varchar(100) NOT NULL,
  `sotien` int(10) NOT NULL,
  `ngay` date NOT NULL,
  `ghichu` varchar(150) NOT NULL,
  `btn-edit` varchar(10) NOT NULL,
  `user` varchar(30) NOT NULL,
  `danhmuc` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `ds_thuchi`
--

INSERT INTO `ds_thuchi` (`id`, `ten`, `sotien`, `ngay`, `ghichu`, `btn-edit`, `user`, `danhmuc`) VALUES
(36, 'tiền điện', -800000, '2020-01-04', 'sdfsdfsdf', '', 'chong', ''),
(38, 'đi chơi cuối tuần1122667788', 200000, '0000-00-00', 'aaaaa', '', '', ''),
(41, 'đi chơi cuối tuần', -200000, '2020-05-17', 'aaaaa', '', 'chong', ''),
(44, 'Tiền nước', -200000, '2020-06-01', 'aaaaa', '', 'chong', ''),
(55, 'Tiềng mạng', -250000, '2020-06-02', '', '', 'chong', ''),
(58, 'Tiền mạng', 300000, '0000-00-00', '', '', 'tiền mạng', ''),
(59, 'Tiền mạng', -300000, '2020-01-02', '', '', 'chong', 'tiền mạng'),
(63, 'Tiền lương vợ', 5000000, '2020-03-01', '', '', 'Vo', 'tiền lương'),
(64, 'Tiền lương chồng', 7000000, '2020-01-01', '', '', 'chong', 'tiền lương'),
(65, 'Tiền lương  vợ', 5000000, '2020-02-01', '', '', 'Vo', 'tiền lương'),
(66, 'Tiền lương chồng', 7000000, '2020-03-01', '', '', 'chong', 'tiền lương'),
(67, 'Tiền lương vợ', 5000000, '2020-04-01', '', '', 'Vo', 'tiền lương'),
(69, 'Tiền lương vợ', 5000000, '2020-05-01', '', '', 'Vo', 'tiền lương'),
(70, 'Tiền lương vợ', 5000000, '2020-06-01', '', '', 'Vo', 'tiền lương'),
(72, 'Tiền lương chồng', 7000000, '2020-04-01', '', '', 'chong', 'tiền lương'),
(73, 'Tiền lương chồng', 7000000, '2020-05-01', '', '', 'chong', 'tiền lương'),
(74, 'Tiền lương chồng', 7000000, '2020-06-01', '', '', 'chong', 'tiền lương'),
(75, 'Tiềng điện', -550000, '2020-06-05', '', '', 'Vo', 'Tiền học của con'),
(76, 'Tiềng mạng', -250000, '2020-01-02', '', '', 'chong', ''),
(77, 'Tiềng mạng', -250000, '2020-02-02', '', '', 'chong', ''),
(78, 'Tiềng mạng', -250000, '2020-03-02', '', '', 'chong', ''),
(79, 'Tiềng mạng', -250000, '2020-04-02', '', '', 'chong', ''),
(80, 'Tiềng mạng', -250000, '2020-05-02', '', '', 'chong', ''),
(81, 'Tiên đi chơi', -550000, '2020-06-02', '', '', 'chong', 'Tiền chi tiêu hàng ngày'),
(82, 'đi du lịch', -4000000, '2020-05-01', '', '', 'chong', 'Tiền chi tiêu hàng ngày');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id_user` int(4) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id_user`, `name`) VALUES
(3, 'chong'),
(4, 'Vo');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `danhmuc`
--
ALTER TABLE `danhmuc`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `ds_thuchi`
--
ALTER TABLE `ds_thuchi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`user`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `danhmuc`
--
ALTER TABLE `danhmuc`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `ds_thuchi`
--
ALTER TABLE `ds_thuchi`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
